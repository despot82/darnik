<?php

$installer = $this;
$installer->startSetup();

$installer->run("UPDATE eav_attribute_option_value eaov 
SET eaov.value=substring(eaov.value, 24, length(eaov.value) - 23)
WHERE eaov.value LIKE 'Nijansa ili pakovanj%';");

Mage::log("'Nijanse ili pakovanje' text has been cut off");

$installer->endSetup();