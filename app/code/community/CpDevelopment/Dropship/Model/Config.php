<?php
class CpDevelopment_Dropship_Model_Config
{
    /**
     * @param string $key
     * @return mixed
     */
    static function getVal($key) {
        return Mage::getStoreConfig("dropship/general/$key");
    }

    /**
     * @param string $key
     * @param mixed $val
     * @return mixed
     */
    static function setVal($key, $val)
    {
        Mage::getConfig()->saveConfig("dropship/general/$key", $val)
            ->cleanCache();
        Mage::app()->reinitStores();
    }

    /**
     * @return bool
     */
    static function isEnabled() {
        return (bool)intval(self::getVal('enable'));
    }

    /**
     * @return bool
     */
    static function isShowButton() {
        return self::isEnabled() && intval(self::getVal('import_show_btn'));
    }

    /**
     * @return bool
     */
    static function isOrderSuccess($bSuccess = false) {
        $v = intval(self::getVal('export_on_ordersuccess'));
        $b_ok = self::isEnabled() && ($bSuccess ^ !$v);
        Mage::log(">isOrderSuccess('$bSuccess'): v='$v'; b_ok='$b_ok'\n");
        return $b_ok;
    }

    /**
     * @return bool
     */
    static function isDebug() {
        return (bool)intval(self::getVal('debug'));
    }

    /**
     * @return int
     */
    static function getImportCurrentLine() {
        return intval(self::getVal('import_current_line'));
    }

    /**
     * @return int
     */
    static function getImportChunk() {
        return 500;
        //return intval(self::getVal('import_chunk'));
    }

}
