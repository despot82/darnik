<?php
class CpDevelopment_Dropship_Model_Api_Client
{
    const XML_DECL = '<?xml version="1.0" encoding="utf-8"?>';
    const URL_ROOT = 'http://dropship_dev.cp-dev.com:1080/export/5';

    const TYPE_JSON = 'application/api.cp-dev.%s+json';
    const TYPE_XML = 'application/api.cp-dev.%s+xml';

    static protected $ACTIONS = array(
        'Create' => 'POST', 'Read' => 'GET', 'Update' => 'PUT', 'Delete' => 'DELETE'
    );

    protected $oHttp = 0;

    /** constructor
     */
    function __construct() {

        $this->dataFile = Mage::getBaseDir() . '/var/dropship-data.txt';

        /* LEGACY code???
        if (!$key) return;
        $this->oHttp = new Zend_Http_Client(null, array(
            'timeout' => 40,
            'adapter' => 'Zend_Http_Client_Adapter_Curl',
            'curloptions' => array(
                //CURLOPT_FOLLOWLOCATION => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
                CURLOPT_USERPWD => $id. ':'. $key,
                CURLOPT_HEADER => true,
                CURLINFO_HEADER_OUT => true,
                CURLOPT_RETURNTRANSFER => true,
            )
        ));

        Mage::log("CpDevelopment_Dropship_Model_Api_Client::__construct('$id', '$key'):\n");
        */

    }

    /**
     * @return Zend_Http_Response
     */
    public function getLastResponse() {
        return $this->oHttp->getLastResponse();
    }

    protected function getData($updated_time = 0, $only_updated = 0)
    {
        set_time_limit(0);

        $api_url = CpDevelopment_Dropship_Model_Config::getVal('api_url');
        $username = CpDevelopment_Dropship_Model_Config::getVal('api_id');
        $password = CpDevelopment_Dropship_Model_Config::getVal('api_key');

        $data = array('username' => $username, 'password' => $password, 'only_updated' => $only_updated);

        Mage::log("DS API call made, with parameters: only_updated: $only_updated | updated_time: $updated_time | url: $api_url", true, 'dropship-api-calls.log');

        if($updated_time)
        {
            $data['updated_time'] = $updated_time;
            $dataFileHandle = fopen(Mage::getBaseDir() . '/var/tmp/dropship-update.txt', 'w+');
        }else
            $dataFileHandle = fopen($this->dataFile, 'w+');

        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $api_url);
            curl_setopt($ch, CURLOPT_FILE, $dataFileHandle);
            curl_setopt($ch, CURLOPT_TIMEOUT, -1); # optional: -1 = unlimited, 3600 = 1 hour
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, 'CURLAUTH_BASIC');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            # Only if you need to bypass SSL certificate validation
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            # Exceute the download - note we DO NOT put the result into a variable!
            $result = curl_exec($ch);

            curl_close($ch);

            if(fstat($dataFileHandle))
            {
                Mage::log("DS API returned data, info on file: ", true, 'dropship-api-calls.log');
                $filestat = fstat($dataFileHandle);
                Mage::log( round($filestat['size'] / (1024*1024), 2) . 'MB' , true, 'dropship-api-calls.log');
            }

            fclose($dataFileHandle);
        } catch (Exception $e) {
            Mage::log("An exception occurred, file CpDevelopment/Dropship/Model/Api/Client.php, line: ".__LINE__.", error: ".$e->getMessage());
            return false;
        }

        return true;
    }

    public function checkResetFlag()
    {
        //curl --data 'store=2' -H 'Authorization: Basic YmFuZTpiYW5l1' http://dropship_dev.cp-dev.com/reset-products
        //{"message":"Products are unreset for Store","reset_products":false,"success":true}
        set_time_limit(0);

        $api_url = CpDevelopment_Dropship_Model_Config::getVal('api_url');
        $reset_check_url = "http://dropship_dev.cp-dev.com/reset-products";
        $store = substr($api_url, strrpos($api_url, '/') + 1);
        $data = array('store' => $store);
        Mage::log("DS check reset flag API call made, with parameters: store: $store", true, 'dropship-api-calls.log');

        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $reset_check_url);
            curl_setopt($ch, CURLOPT_FILE, $dataFileHandle);
            curl_setopt($ch, CURLOPT_TIMEOUT, -1); # optional: -1 = unlimited, 3600 = 1 hour
            //curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, 'CURLAUTH_BASIC');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            # Only if you need to bypass SSL certificate validation
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $result = curl_exec($ch);
            curl_close($ch);
            return json_decode($result, true);
        } catch (Exception $e) {
            Mage::log("An exception occurred, file CpDevelopment/Dropship/Model/Api/Client.php, line: ".__LINE__.", error: ".$e->getMessage());
            return false;
        }
    }

    public function resetFlag()
    {
        //curl --data 'store=2&unreset=1' -H 'Authorization: Basic YmFuZTpiYW5l1' http://dropship_dev.cp-dev.com/reset-products
        //{"message":"Products are unreset for Store","reset_products":false,"success":true}
        set_time_limit(0);

        $api_url = CpDevelopment_Dropship_Model_Config::getVal('api_url');
        $reset_check_url = "http://dropship_dev.cp-dev.com/reset-products";
        $store = substr($api_url, strrpos($api_url, '/') + 1);
        $data = array('store' => $store, 'unreset' => 1);
        Mage::log("DS check reset flag API call made, with parameters: store: $store", true, 'dropship-api-calls.log');

        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $reset_check_url);
            curl_setopt($ch, CURLOPT_FILE, $dataFileHandle);
            curl_setopt($ch, CURLOPT_TIMEOUT, -1); # optional: -1 = unlimited, 3600 = 1 hour
            //curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, 'CURLAUTH_BASIC');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            # Only if you need to bypass SSL certificate validation
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $result = curl_exec($ch);
            curl_close($ch);
            return json_decode($result, true);
        } catch (Exception $e) {
            Mage::log("An exception occurred, file CpDevelopment/Dropship/Model/Api/Client.php, line: ".__LINE__.", error: ".$e->getMessage());
            return false;
        }
    }

    public function sendProductUrls($unsynced)
    {
        if(!is_array($unsynced) || empty($unsynced))
            return false;
    
        $api_url = CpDevelopment_Dropship_Model_Config::getVal('api_url');
        // we extract user interface from api url
        $user_interface_id = substr($api_url, strrpos($api_url, '/') + 1);

        set_time_limit(0);
        $sendProductUrlsAPIUrl = 'http://dropship_dev.cp-dev.com:1080/import-products-urls/'.$user_interface_id;

        // testing:
        //$unsynced = array('2342234243234' => '/test1.html', '45332424566' => '/test2');
        //$sendProductUrlsAPIUrl = 'http://dropship_dev.cp-dev.com:1080/import-products-urls/2';
        // end testing

        $products = json_encode($unsynced);
        $data = array('products_urls' => $products);

        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $sendProductUrlsAPIUrl);
            curl_setopt($ch, CURLOPT_TIMEOUT, -1); # optional: -1 = unlimited, 3600 = 1 hour
            //curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, 'CURLAUTH_BASIC');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            # Only if you need to bypass SSL certificate validation
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            # Exceute the download - note we DO NOT put the result into a variable!
            curl_exec($ch);
            curl_close($ch);
        } catch (Exception $e) {
            Mage::log("An exception occurred, file CpDevelopment/Dropship/Model/Api/Client.php, line: ".__LINE__.", error: ".$e->getMessage());
            return false;
        }

        return true;
    }

    /** Notifies DS API gateway of deleted products
     *
     * @param array $product_ids
     */
    public function sendDeletedProductIds($product_ids)
    {
        if(!is_array($product_ids) || empty($product_ids))
            return false;
    
        $api_url = CpDevelopment_Dropship_Model_Config::getVal('api_url');
        // we extract user interface from api url
        $user_interface_id = substr($api_url, strrpos($api_url, '/') + 1);

        set_time_limit(0);
        $update_discontinued = 'http://dropship_dev.cp-dev.com:1080/delete-discontinued-products';
        //hardcoded
        $password = 'blaTrt76';//CpDevelopment_Dropship_Model_Config::getVal('api_key');

        $products = '[' . implode(',', $product_ids) . ']';

        $data = array('user_interface_id' => $user_interface_id, 'extended_password' => $password, 'products' => $products);

        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $update_discontinued);
            curl_setopt($ch, CURLOPT_TIMEOUT, -1); # optional: -1 = unlimited, 3600 = 1 hour
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, 'CURLAUTH_BASIC');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            # Only if you need to bypass SSL certificate validation
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            # Exceute the download - note we DO NOT put the result into a variable!
            curl_exec($ch);
            curl_close($ch);
        } catch (Exception $e) {
            Mage::log("An exception occurred, file CpDevelopment/Dropship/Model/Api/Client.php, line: ".__LINE__.", error: ".$e->getMessage());
            return false;
        }

        return true;
    }


    /** LEGACY???
     *
     * @param string $sUri - resource URI
     * @param string $sResrc - resource name
     * @param string $sAction - action
     * @param bool $bJson - set content-type to JSON (instead of XML)
     * @param array|string $data - POST request data
     * @return array|string|false
     */
    protected function request($sUri, $sResrc, $sAction = 0, $bJson = true, $data = 0) 
    {

        if (!$this->oHttp) return false;

        Mage::log("CpDevelopment_Dropship_Model_Api_Client::request(): params=('$sUri', '$sResrc', '$sAction', '$bJson')\n data: ".rtrim(print_r($data,true))."\n");

        $url = self::fixUrl($sUri);
        $this->oHttp->setUri($url);
        if (isset(self::$ACTIONS[$sAction]))
            $this->oHttp->setMethod(self::$ACTIONS[$sAction]);
        $type = sprintf($bJson? self::TYPE_JSON : self::TYPE_XML, $sResrc);
        $this->oHttp->setHeaders('Accept', $type);
        if ($data) {
            $this->oHttp->setRawData($bJson? json_encode($data) : $data)
                ->setHeaders('Content-Type', $type);
        }
        $s_resp = $this->oHttp->request()->getBody();
        Mage::log("\n Request Headers:\n".rtrim($this->oHttp->getLastRequest())."\n Response Headers:\n".rtrim($this->oHttp->getLastResponse()->getHeadersAsString(true))."\n");
        if (!$bJson) Mage::log(" Response(".strlen($s_resp)."):\n$s_resp\n");
        return $s_resp? ($bJson? json_decode($s_resp, true) : $s_resp) : false;

    }

    static protected function fixUrl($url) {
        return (!$url || self::isUrl($url))? $url : self::URL_ROOT. $url;
    }

    static protected function isUrl($url) {
        return strpos($url, 'http') === 1;
    }

    /**
     * @param array $arr
     * @param array $tagWrap
     * @return string
     */
    static protected function buildPlainXml($arr, $tagWrap = '') {
        $out = '';
        foreach ($arr as $key => $val)
            $out .= "<$key>$val</$key>";
        return $tagWrap? "<$tagWrap>$out<$tagWrap>" : $out;
    }
}
