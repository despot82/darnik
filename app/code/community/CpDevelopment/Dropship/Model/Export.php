<?php

class CpDevelopment_Dropship_Model_Export extends CpDevelopment_Dropship_Model_Api_Client
{
	static protected $isExportRunning = false;

	function __construct() 
	{
		ini_set('memory_limit', '768M');

		$this->isEnabled = CpDevelopment_Dropship_Model_Config::isEnabled();

		if (!$this->isEnabled) 
			return;

		parent::__construct($id, $key);
	}

	/**
	 * @return bool
	 */
	public function runProductUrlExport()
	{
		$this->isEnabled = CpDevelopment_Dropship_Model_Config::isEnabled();

		if (!$this->isEnabled) 
			return;

		if(self::$isExportRunning)
		{
			Mage::log('Aborting, another instance of export cronjob is already running ... ');
			return false;
		}else{
			self::$isExportRunning = true;
		}

		/* @var $collection Mage_Catalog_Model_Resource_Category_Collection */
		$collection = Mage::getResourceModel('catalog/product_collection')
			->addAttributeToSelect('ds_is_product_synced')
			->addAttributeToSelect('ds_dropship_product_id')
			->addAttributeToSelect('url_path')
			->addFieldToFilter('visibility', array(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH))
			->addFieldToFilter('status', 1);
		
		// Do the work here
		$unsynced = array();
        $products = array();

		foreach($collection as $p)
		{
			$ds_id= $p->getData('ds_dropship_product_id');
			$is_synced = $p->getData('ds_is_product_synced');
			$url = $p->getData('url_path');

			if($ds_id && !$is_synced)
			{
				$unsynced[] = array( $ds_id => '/'.$url );
                $products[] = $p;

				// importing in the chunks of 1000 products
				if(count($unsynced) > 1000)
				{
					if($this->sendProductUrls($unsynced))
                    {
                        foreach($products as $product)
                        {
                            //echo $product->getSku(),"\n";
                            $product->setData('ds_is_product_synced', '1')->save();
                        }
                    }

					unset($unsynced);
                    unset($products);
					$unsynced = array();
                    $products = array();
				}
			}
		}
		// end work

		// release the grip
		self::$isExportRunning = false;
	}
}