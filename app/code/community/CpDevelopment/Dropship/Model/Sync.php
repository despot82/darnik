<?php

class CpDevelopment_Dropship_Model_Sync extends CpDevelopment_Dropship_Model_Api_Client
{
    const MANUFACTURER_LOGO_DIR = 'manufacturer-logo';
    const URI_PRODUCTS = '/products?limit=%s&offset=%s';
    const URI_ORDERS = '/orders';
    const N_MAXPAGES = 0;
    const ATTR_PREFIX = 'ds_';
    const ATTR_SET_NAME = 'Dropship Products';
    const ATTR_GROUP_NAME = 'Dropship Product Fields';
    const B_POSTJSON = false;
    const XML_ORDER = '<order>
        <po_number></po_number>
        <notes></notes>
        <ship_options>%s</ship_options>
        <ship_to_address>%s</ship_to_address>
        <items>%s</items>
    </order>';

    static protected $A_ATTRS = array(
        'modified' => array(
            'label' => 'Modified at',
            'data' => array('frontend_input' => 'text',),
        ),
        'manufacturer_logo' => array(
            'label' => 'Manufacturer logo',
            'data' => array('frontend_input' => 'text', 'apply_to' => array('simple','configurable'),),
        ),
        'manufacturer' => array(
            'label' => 'Brand',
            'data' => array('frontend_input' => 'select', 'is_visible_on_front' => '1', 'is_searchable' => '1', 'is_filterable' => 1, 'is_filterable_in_search' =>1, 'apply_to' => array('simple','configurable'),),
        ),
        'dropship_product_id' => array(
            'label' => 'Dropship Product ID',
            'data' => array(
                'frontend_input' => 'text',
                'apply_to' => array('simple','configurable'),
                'is_unique' => '0',
                'is_required' => '1',
                'default_value' => 'N/A',
            ),
        ),
        'upc' => array(
            'label' => 'UPC',
            'data' => array(
                'frontend_input' => 'text',
                'apply_to' => array('simple','configurable'),
                'is_unique' => '1',
                'is_required' => '0',
            ),
        ),
        'ean' => array(
            'label' => 'EAN',
            'data' => array(
                'frontend_input' => 'text',
                'apply_to' => array('simple','configurable'),
                'is_unique' => '1',
                'is_required' => '0',
            ),
        ),
        'supplier'   => array(
            'label' => 'Supplier',
            'data' => array(
                'frontend_input' => 'text',
                'apply_to' => array('simple','configurable'),
                'is_unique' => '0',
                'is_required' => '0',
            ),
        ),

        'weight'   => array(
            'label' => 'Real Weight',
            'data' => array(
                'frontend_input' => 'text',
                'apply_to' => array('simple','configurable'),
                'is_unique' => '0',
                'is_required' => '0',
            ),
        ),
        'height'   => array(
            'label' => 'Height',
            'data' => array(
                'frontend_input' => 'text',
                'apply_to' => array('simple','configurable'),
                'is_unique' => '0',
                'is_required' => '0',
            ),
        ),
        'depth'   => array(
            'label' => 'Depth',
            'data' => array(
                'frontend_input' => 'text',
                'apply_to' => array('simple','configurable'),
                'is_unique' => '0',
                'is_required' => '0',
            ),
        ),
        'shipping_per_pound'   => array(
            'label' => 'Shipping Per Pound',
            'data' => array(
                'frontend_input' => 'text',
                'apply_to' => array('simple','configurable'),
                'is_unique' => '0',
                'is_required' => '0',
            ),
        ),
        'shipping_fixed'   => array(
            'label' => 'Shipping Fixed',
            'data' => array(
                'frontend_input' => 'text',
                'apply_to' => array('simple','configurable'),
                'is_unique' => '0',
                'is_required' => '0',
            ),
        ),
        'calculated_shipping'   => array(
            'label' => 'Calculated Shipping',
            'data' => array(
                'frontend_input' => 'text',
                'apply_to' => array('simple','configurable'),
                'is_unique' => '0',
                'is_required' => '0',
            ),
        ),

        'shipping_days'   => array(
            'label' => 'Shipping Days',
            'data' => array(
                'frontend_input' => 'text',
                'apply_to' => array('simple','configurable'),
                'is_unique' => '0',
                'is_required' => '0',
            ),
        ),
        'shipping_minimum_days'   => array(
            'label' => 'Shipping Minimum Days',
            'data' => array(
                'frontend_input' => 'text',
                'apply_to' => array('simple','configurable'),
                'is_unique' => '0',
                'is_required' => '0',
            ),
        ),
    );

    static protected $A_MEDIA_ATTRS = array('image', 'small_image', 'thumbnail');
    static protected $A_CARRIERS = array('FEDEX', 'UPS');
    static protected $A_METHODS = array('GROUND', 'PRIORITY', 'OVERNIGHT');

    protected $isEnabled = false;
    protected $idAttrSet = false;
    protected $idAttrGroup = false;
    protected $aAttrIds = false;
    protected $aAttrOptionsets = array();
    static protected $isImportRunning = false;

    function __construct()
    {
        ini_set('memory_limit', '768M');

        $this->isEnabled = CpDevelopment_Dropship_Model_Config::isEnabled();
        $this->isDebug = CpDevelopment_Dropship_Model_Config::isDebug();
        $this->currentLine = CpDevelopment_Dropship_Model_Config::getImportCurrentLine();
        $this->importChunk = CpDevelopment_Dropship_Model_Config::getImportChunk();

        if (!$this->isEnabled)
            return;

        parent::__construct();

        $id = $this->findAttrSet(self::ATTR_SET_NAME);

        if(!$id)
            $id = $this->createAttrSet(self::ATTR_SET_NAME);

        if($id)
            $this->idAttrSet = $id;

        $id = $this->findAttrGroup(self::ATTR_GROUP_NAME, $this->idAttrSet);

        if(!$id)
            $id = $this->createAttrGroup(self::ATTR_GROUP_NAME, $this->idAttrSet);

        if($id)
            $this->idAttrGroup = $id;

        if (!$this->idAttrSet || !$this->idAttrGroup)
            return;

        foreach (self::$A_ATTRS as $name => $arr)
        {
            $code = self::ATTR_PREFIX. $name;

            if (!$this->loadAttrId($code))
                $this->createAttr($code, $arr['label'], $arr['data']);

            if (!isset($arr['is_select']))
                continue;

            $this->loadAttrOptions($code);
        }


        // This needs to be run only once, so a built-in safe mechanism is to skip it after 5th of July of 2016
        if(time() > 1467319064)
            return;

        // we have to add new (permanent attributes on existing installations)
        $attribute_codes = array(
            'ds_supplier',
            'ds_weight',
            'ds_height',
            'ds_depth',
            'ds_shipping_per_pound',
            'ds_shipping_fixed',
            'ds_calculated_shipping',
            'ds_shipping_days',
            'ds_shipping_minimum_days'
        );

        $attributeGroups = Mage::getResourceModel('eav/entity_attribute_group_collection');

        foreach($attributeGroups as $ag)
        {
            $gData = $ag->getData();

            if($gData['attribute_group_name'] == 'Dropship Product Fields')
            {
                foreach($attribute_codes as $acode)
                {
                    $id = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', $acode);
                    Mage::getModel('eav/entity_setup','core_setup')->addAttributeToSet('catalog_product',$gData['attribute_set_id'],$gData['attribute_group_id'], $id);
                }
            }
        }
        // end of one time run

    }

    /**
     * @return bool
     */
    public function runProductImport()
    {
        if(self::$isImportRunning)
        {
            Mage::log('Aborting, another instance of import_dropship cronjob is already running ... ');
            return false;
        }else{
            self::$isImportRunning = true;
        }

        if (!$this->isEnabled)
            return false;

        if (!$this->idAttrSet || !$this->idAttrGroup)
            return false;

        $this->_stopReindex();

        try {
            $this->procImport();
        } catch (Exception $obj) {
            Mage::log("\n>Exception(runProductImport): msg(".$obj->getCode()."): ".$obj->getMessage()."\n");
        }

        $indexCollection = Mage::getModel('index/process')->getCollection();

        foreach ($indexCollection as $index)
            $index->reindexAll();

        $this->_startReindex();

        // release the grip
        self::$isImportRunning = false;
    }

    /**
     * @return array|false
     */
    protected function procImport()
    {
        $res = $this->checkResetFlag();
        //print_r($res); exit;

        if($res['reset_products'])
        {
            CpDevelopment_Dropship_Model_Config::setVal('import_current_line', 0);
            CpDevelopment_Dropship_Model_Config::setVal('first_time_fetch', 1);
            $this->currentLine = 0;
            $res = $this->resetFlag();
        }

        //Mage::log(CpDevelopment_Dropship_Model_Config::getVal('first_time_fetch'), true, 'martin.log');

        // fetch data into predefined file (var/dropship-data.txt)
        //if(!stristr(Mage::getBaseUrl(), 'dev.dropship.com') && !$this->currentLine)
        if(!$this->currentLine)
        {
            // first time fetch? do not use 'only_updated'
            $only_updated = CpDevelopment_Dropship_Model_Config::getVal('first_time_fetch') ? 0 : 1;
            // get only updated
            $this->getData(0,$only_updated);
        }

        // we read line by line from data file
        $dataFile = fopen($this->dataFile, 'r');
        $this->_initCategories();

        if($dataFile)
        {
            // if nothing from the file has been processed yet:
            if(!$this->currentLine)
            {
                while(($line = fgets($dataFile)) !== false)
                {
                    $d = json_decode($line, true);

                    if(!empty($d['Discontinued']))
                        $this->deleteProductsSkus($d['Discontinued']);

                    if(!empty($d['DeleteCategories']))
                    {
                        $this->deleteCategories($d['DeleteCategories']);
                        // reinitialize categories as the structure changed
                        $this->_initCategories();
                    }

                    if(!empty($d['Categories']) && !isset($d['Product_ID']))
                    {
                        if(!$this->initCategories($d['Categories']))
                        {
                            //echo "Something went wrong while synchronizing category tree. Aborting import ... \n";
                            Mage::log("Something went wrong while synchronizing category tree. Aborting import ... ");
                            return false;
                        }

                        break;
                    }
                }
            }

            rewind($dataFile);

            $this->line_counter = 0;
            $this->starting_line = $this->currentLine;
            $this->ending_line = $this->currentLine + $this->importChunk;
            $this->current_time = microtime(true);
            $warnNoShippingFlag = 0;

            while(($line = fgets($dataFile)) !== false)
            {
                $this->line_counter++;

                if($this->line_counter < $this->starting_line)
                    continue;

                $d = json_decode($line, true);

                if(!empty($d['Product_ID']))
                {
                    if(!empty($d['UPC']))
                        $d['Attributes']['UPC'] = $d['UPC'];

                    if(empty($d['CalculatedShipping']) && $d['CalculatedShipping'] !== '0' && $d['CalculatedShipping'] !== 0)
                        $warnNoShippingFlag = 1;

                    if($this->isDebug)
                    {
                        Mage::log("~~~~~~~~~~~~~ ADDING ATTRIUBTES AND ATTRIBUTE SETS/GROUPS ( SKU = {$d['Product_ID']} ) ~~~~~~~~~~~~~", true, 'dropship.log');
                        $attrStartTime = microtime(true);
                    }

                    // now we create attributes that don't exist
                    foreach($d['Attributes'] as $attribute)
                    {
                        $code = strip_tags($attribute['name']);
                        $code = self::ATTR_PREFIX . str_replace(' ', '_', preg_replace('/[^a-z ]/','', strtolower($code)));

                        // attribute code must not be longer then 30 chars
                        $code = substr($code, 0, 29);

                        $attrId = $this->loadAttrId($code);
                        $label = strip_tags($attribute['name']);

                        if (!$attrId)
                        {
                            $data = array('frontend_input' => 'select', 'is_visible_on_front' => '1', 'is_searchable' => '1', 'is_filterable' => 1, 'is_filterable_in_search' =>1, 'apply_to' => array('simple','configurable'),);
                            //$data = array('is_visible_on_front' => '1', 'is_searchable' => '1', 'apply_to' => array('simple','configurable'),);
                            $attrId = $this->createAttr($code, $label, $data); // we don't actually use attribute id
                        }
                    }

                    if($this->isDebug)
                        Mage::log("Created all product attributes in: " . (microtime(true) - $attrStartTime) . "s", true, 'dropship.log');

                    unset($code);
                    unset($attribute);

                    if(!empty($d['Variations']) and count($d['Variations']) > 1)
                    {
                        $code = self::ATTR_PREFIX. 'variations_'.$d['Product_ID'];

                        // attribute code must not be longer then 30 chars
                        $code = substr($code, 0, 29);

                        $configAttrId = $this->createConfigAttrs(array('name' => 'Variations', 'code' => 'variations_'.$d['Product_ID']), false);
                        $this->variationAttributes[$code]['attrId'] = $configAttrId;

                        if($this->isDebug)
                            $attrStartTime = microtime(true);

                        foreach($d['Variations'] as $aOption)
                        {
                            // here we create a value for all variations
                            $label = '';

                            if(array_key_exists('Attributes', $aOption))
                            {
                                foreach($aOption['Attributes'] as $aAttr)
                                {
                                    $a_value = strip_tags($aAttr['value']);
                                    $a_name = strip_tags($aAttr['name']);
                                    $label .= $a_name.': '.$a_value.', ';
                                }
                            }

                            if(empty($label))
                                $label = $d['Title'] . ' - ' . $aOption['Product_ID'];

                            $default_label = rtrim($label, ', ');
                            $label = $default_label . ' (' . $aOption['Product_ID'] . ')';
                            $this->variationAttributes[$code]['values'][$aOption['Product_ID']] = $this->getProductAttributeValue($code, $label, $default_label);
                        }

                        if($this->isDebug)
                            Mage::log("Created all product attributes (these are for Variations) in: " . (microtime(true) - $attrStartTime) . "s", true, 'dropship.log');

                        if($this->isDebug)
                            $startTime = microtime(true);

                        // now let's create attribut set for this product
                        //1. create attribute set based on Dropship atribute set and attribute group
                        $in_setId = $this->findAttrSet(self::ATTR_SET_NAME . ' - ' . $d['Product_ID']);

                        if(!$in_setId)
                            $in_setId = $this->createAttrSet(self::ATTR_SET_NAME . ' - ' . $d['Product_ID'], self::ATTR_SET_NAME);

                        $in_groupId = $this->findAttrGroup(self::ATTR_GROUP_NAME, $in_setId);

                        //2. add the configurable atribut to this atribut set
                        Mage::getModel('eav/entity_setup','core_setup')->addAttributeToSet('catalog_product',$in_setId,$in_groupId,$configAttrId);

                        if($this->isDebug)
                            Mage::log("Created product attribute set/group (these are for Variations) in: " . (microtime(true) - $startTime) . "s", true, 'dropship.log');

                        //3. this atribut set needs to be passed along when creating variation this config product and its variations:
                        $d['attribute_set_id'] = $in_setId;

                        $newConfigSwatchesIds[] = $configAttrId;
                    }

                    if($this->isDebug)
                        Mage::log("~~~~~~~~~~~~~ END ~~~~~~~~~~~~~", true, 'dropship.log');

                    $product_id = $this->procProduct($d);
                }

                $this->current_time = microtime(true);
                CpDevelopment_Dropship_Model_Config::setVal('import_current_line', $this->line_counter);

                if($this->line_counter > $this->ending_line)
                    break;
            }

            // enable configurable swatches
            if(!Mage::getStoreConfig('configswatches/general/enabled'))
                Mage::getConfig()->saveConfig('configswatches/general/enabled', '1', 'default', 0);

            // adding new attribute to configurable swatches
            $configSwatchesIds = explode(',', Mage::getStoreConfig('configswatches/general/swatch_attributes'));

            foreach($newConfigSwatchesIds as $ncsId)
                if(!in_array($ncsId, $configSwatchesIds))
                    $configSwatchesIds[] = $ncsId;

            Mage::getConfig()->saveConfig('configswatches/general/swatch_attributes', implode(',', $configSwatchesIds), 'default', 0);
            Mage::app()->getCacheInstance()->cleanType('config');

            if(feof($dataFile))
            {
                //echo "Entering the postprocess code segment. We need to send here the maximum updated date...\n";

                rewind($dataFile);
                $time = false;
                $cnt = 0;

                // just quickly find max value from "Updated" fields
                while(($line = fgets($dataFile)) !== false)
                {
                    $d = json_decode($line, true);

                    // First we check if there was an error with access
                    // NOTE/SUGGESTION: this really should be done elsewhere, before any of this code
                    // 
                    //{"Categories":"There is no categories for UserinterfaceID: 836","success":false}

                    if(empty($d['success']) && !empty($d['reason']) && $cnt == 0)
                    {
                        CpDevelopment_Dropship_Model_Config::setVal('import_current_line', 0);

                        $subject = "API returned error while executing DS import";
                        $body = "API user: " . CpDevelopment_Dropship_Model_Config::getVal('api_id') . "\n" .
                                "API url: " . CpDevelopment_Dropship_Model_Config::getVal('api_url') . "\n" .
                                "Message: " . $d['message'] . "\n" .
                                "Reason: " . $d['reason'];


                        Mage::log('ERROR. Message: ' . $d['message'] . ', reason: ' . $d['reason'], true, 'dropship-error.log');

                        $receiver = "dropshiptech@cp-dev.com";
                        mail($receiver, $subject, $body);
                        return;
                    }

                    $cnt++;

                    if(isset($d["Updated"]) && !empty($d["Updated"]))
                    {
                        $lineUpdate = $d["Updated"];
                        //echo $lineUpdate,"\n";

                        if(!$time or strtotime($lineUpdate) > strtotime($time))
                            $time = $lineUpdate;
                    }
                }

                //echo "Max time: $time\n";

                if($time)
                    $this->getData($time,0);

                /*
                $time = date("Y-m-d H:i:s", time());
                $this->getData($time,0);
                */
                // reseting the value to 0 so it can start doing the updates
                CpDevelopment_Dropship_Model_Config::setVal('import_current_line', 0);
                CpDevelopment_Dropship_Model_Config::setVal('first_time_fetch', 0);

                if($warnNoShippingFlag)
                {
                    $subject = "No CalculatedShipping value";
                    $body = "API user: " . CpDevelopment_Dropship_Model_Config::getVal('api_id') ."\n".
                            "API url: " . CpDevelopment_Dropship_Model_Config::getVal('api_url');
                    $receiver = "dropshiptech@cp-dev.com";

                    mail($receiver, $subject, $body);
                }
            }

            fclose($dataFile);
        }
    }

    protected function deleteProductsSkus($skus)
    {
        try{
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $query = "DELETE FROM `catalog_product_entity` WHERE `sku` IN " . '("' . implode('","', $skus) . '")';
            $writeConnection->query($query);
        } catch(Exception $e) {
            Mage::log("Failed to delete discontinued products, reported error: " . $e->getMessage());
            return false;
        }

        // products were successfully deleted, notify API
        $this->sendDeletedProductIds($skus);
    }

    /**
     * Deletes Magento categories with property DS Category ID begining with values from dsCategoryIds
     *
     * @param array $dsCategoryIds
     * @return boolean
     */
    protected function deleteCategories($dsCategoryIds)
    {
        $mag_cat_ids = $this->getCategoryIds($dsCategoryIds);

        foreach ($mag_cat_ids as $cat_id)
        {
            $category = Mage::getModel('catalog/category')->load($cat_id);
            $category->delete();
        }
    }

    protected function createConfigAttrs($attr, $addToDefaultSetId = true)
    {
        $code = self::ATTR_PREFIX. $attr['code'];
        $attrId = $this->loadAttrId($code);

        if (!$attrId)
        {
            $data = array('frontend_input' => 'select', 'is_visible_on_front' => '1', 'is_searchable' => '1', 'is_configurable' => '1', 'apply_to' => array('simple','configurable'),);
            $attrId = $this->createAttr($code, $attr['name'], $data, $addToDefaultSetId); // we don't actually use attribute id
            return $attrId;
        }

        return $attrId;
    }

    /**
     * Tries to find product attribute id based on attribute code and attribute label
     * If not found, creates/adds new value (this works with dropdown and multiple
     * select types)
     *
     * @param string $attr_code
     * @param string $attr_label
     * @return integer
     */
    function getProductAttributeValue($attr_code, $attr_label, $default_label = '')
    {
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attr_code);
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
        }

        foreach($options as $option)
            if ($option['label'] == $attr_label)
                return $option['value'];

        $attribute->setData('option', array(
                'value' => array(
                    'option' => array($attr_label, $default_label)
                    )
            ));

        $attribute->save();
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attr_code);

        //print_r($attribute->getSource()->getAllOptions(false));

        foreach($attribute->getSource()->getAllOptions(false) as $option)
            if ($option['label'] == $attr_label)
                return $option['value'];

        return false;
    }

    public function _initCategories()
    {
        $this->_categories = array();

        $collection = Mage::getResourceModel('catalog/category_collection')
            ->addAttributeToSelect('*');

        /* @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection */
        foreach ($collection as $category)
        {
            $structure = explode('/', $category->getPath());
            $pathSize = count($structure);

            if ($pathSize > 1)
            {
                $path = array();
                for ($i = 1; $i < $pathSize; $i++)
                {

                    if(!empty($structure[$i]))
                    {
                        if(is_object($collection->getItemById($structure[$i])))
                            $path[] = $collection->getItemById($structure[$i])->getName();
                    }
                }

                $rootCategoryName = array_shift($path);

                if (!isset($this->_categoriesWithRoots[$rootCategoryName]))
                    $this->_categoriesWithRoots[$rootCategoryName] = array();

                $index = implode('/', $path);

                $this->_categoriesWithRoots[$rootCategoryName][$index] = $category->getId().':'.$category->getDsCategoryId();

                if ($pathSize > 2)
                {
                    $this->_categories[$index] = $category->getId().':'.$category->getDsCategoryId();
                    $this->_ds_categories[$category->getId()] = $category->getDsCategoryId();
                }
            }
        }
    }

    /**
     *  Kind of normalizes the categories data so there's no situation where child category comes
     *  before parent in the sequence of creating/initializing category tree. Recurrent function.
     *
     *  @param array $categories - initial category array received from DS API
     *  @param array $new_categories - this is the array that will hold the actual end result
     *  @return array
     */
    protected function rearrangeCategories($categories, $new_categories = null)
    {
        if(empty($new_categories))
            $new_categories = array();

        if(!$this->nest)
            $this->nest = 1;
        else
            $this->nest++;

        foreach($categories as $key => $val)
        {
            // if val['parent-id'] exists and is not in new_categories, enter recurrency
            if(!empty($val['Parent_ID']))
            {
                $is_in = 0;

                foreach($new_categories as $nkey => $nval)
                    if($nval['Category_ID'] == $val['Parent_ID'])
                        $is_in = 1;

                if($is_in)
                {
                    $new_categories[] = $val;
                    unset($categories[$key]);
                }
            }else{
                $new_categories[] = $val;
                unset($categories[$key]);
            }
        }

        $more = count($categories);

        if($more && $this->nest < 12)
            $new_categories = $this->rearrangeCategories($categories, $new_categories);

        return $new_categories;
    }

    /**
     * Initializes/syncs Magento category tree with the data received from Dropship API
     *
     * @param array $categories
     * @return bool
     */
    protected function initCategories($categories)
    {
        $categories = $this->rearrangeCategories($categories);

        foreach($categories as $cat)
        {
            $id = $cat['Category_ID'];
            $name = $cat['Title'] ? $cat['Title'] : $cat['Category_ID'];
            $dsCategoryId = $cat['Category_ID'];
            $position = array();

            if(!empty($cat['Parent_ID']))
                $dsCategoryId = $dsCategoryId . '.' . $cat['Parent_ID'];

            // 1) try to find if this category already exists
            $catIds = array_keys($this->_ds_categories, $dsCategoryId);

            if(empty($catIds))
            {
                // no category, create one

                // 1.1) find parent(s) and for each parent create a new subcategory based on $cat data
                $parent_ids = array();

                foreach($this->_ds_categories as $magentoCatId => $dsCat)
                {
                    $firstPID = $dsCat;

                    if(strstr($dsCat, '.'))
                        $firstPID = substr($dsCat, 0, strpos($dsCat, '.'));

                    if($firstPID == $cat['Parent_ID'])
                        $parent_ids[] = $magentoCatId;
                }

                if(!empty($parent_ids))
                {
                    foreach($parent_ids as $parent_id)
                    {
                        if(empty($position[$parent_id]))
                            $position[$parent_id] = 1;
                        else
                            $position[$parent_id]++;

                        $this->createCategory($dsCategoryId, $name, $parent_id, $position[$parent_id]);
                    }
                }else{
                    $this->createCategory($dsCategoryId, $name, 0, 1);
                }
            }elseif(count($catIds) > 1){
                // more then one category with identical ds category id? something's wrong, report

                $subject = "Category tree import/sync error while executing DS import";
                $body = "API user: " . CpDevelopment_Dropship_Model_Config::getVal('api_id') . "\n" .
                        "API url: " . CpDevelopment_Dropship_Model_Config::getVal('api_url') . "\n" .
                        "Message: Found multiple (".count($catIds)." of them) categories with identical DS category ids (formed as [Category_ID].[Parent_ID]). This DS category id is: $dsCategoryId\n";


                Mage::log('ERROR. Message: ' . $body, true, 'dropship-error.log');

                $receiver = "dropshiptech@cp-dev.com";
                mail($receiver, $subject, $body);
                die();
            }
        }

        return true;
    }

    public function createCategory($dsCategoryId, $name, $parentId = 0, $position)
    {
        $storeId = 1;

        if(!$parentId)
            $parentId = Mage::app()->getStore($storeId)->getRootCategoryId();

        $parentCategory = Mage::getModel('catalog/category')->load($parentId);

        try{
            $category = Mage::getModel('catalog/category');
            $category->setName($name)
                ->setDsCategoryId($dsCategoryId)
                ->setPosition($position)
                ->setStoreId($storeId)
                ->setIsActive(1)
                ->setDisplayMode(Mage_Catalog_Model_Category::DM_PRODUCT)
                ->setIsAnchor(1)
                ->setLevel($parentCategory->getLevel() + 1)
                ->setCreatedAt(date('Y-m-d H:i:s'))
                ->save();

            $category->setPath($parentCategory->getPath().'/'.$category->getId())->save();

            $this->_ds_categories[$category->getId()] = $dsCategoryId;

        } catch(Exception $e) {
            Mage::log($e->getMessage(), true, 'dropship-error.log');
            return false;
        }

        return true;
    }

    protected function addImages($product_id, $aData)
    {
        $o_prod = Mage::getModel('catalog/product')->load($product_id);
        $image_arr = array();

        if(array_key_exists('Images', $aData))
            foreach($aData['Images'] as $img)
                $image_arr[] = $img;

        // adding any variation images to main configurable image
        if(array_key_exists('Variations', $aData))
        {
            foreach($aData['Variations'] as $vImages)
            {
                foreach($vImages['Images'] as $vimg)
                {
                    $vImgUrl = $vimg;

                    if(in_array($vImgUrl, $image_arr))
                        continue;
                    else
                        $image_arr[] = $vImgUrl;
                }
            }
        }

        $aData['Images'] = $image_arr;

        if($this->isDebug)
        {
            Mage::log("BEGIN", true, 'dropship.log');
            $fTime = microtime(true);
        }

        foreach($image_arr as $imgUrl)
        {
            if($this->isDebug)
                $iTime = microtime(true);

            $path_img = $this->saveImage($imgUrl);
            //Mage::log("Saved product image (download and save to disk time essentially) in: " . (microtime(true) - $iTime)  . "s (url: $imgUrl)", true, 'dropship.log');

            if($i == 0)
                $attrs = array('image','thumbnail','small_image');
            else
                $attrs = array();

            $i++;

            if(file_exists($path_img))
                $o_prod->addImageToMediaGallery($path_img, $attrs, true, false);
            else
                Mage::log('Image on provided url ('.$imgUrl.') doesn\'t seem to exist. Related product sku: ' . $o_prod->getSku());

            if($this->isDebug)
            {
                $t = intval((microtime(true) - $iTime) * 1000) . ' ms';
                Mage::log("Added single image to product in: $t", true, 'dropship.log');
            }
        }

        try{
            $o_prod->save();
        } catch (Exception $e) {
            Mage::log('File: CpDevelopment_Dropship_Model_Sync, line: '. __LINE__ . ', exception: ' . $e->getMessage());
        }

        if($this->isDebug)
        {
            $t = intval((microtime(true) - $fTime) * 1000) . ' ms';
            Mage::log("", true, 'dropship.log');
            Mage::log("Added all product images in: $t", true, 'dropship.log');
            Mage::log("END", true, 'dropship.log');
        }
    }

    protected function procProduct($aData, $visibility = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH, $assoc = false)
    {
        if (!is_array($aData))
            return false;

        $aData = array_map(
            function ($v) { return is_array($v)? $v : trim($v); },
            $aData
        );

        $product_id = Mage::getModel('catalog/product')->getIdBySku($aData['Product_ID']);

        if ($product_id)
        {
            $pid = $this->updateProduct($product_id, $aData, $visibility);
            sleep(2);
            return $pid;
        }else{
            if($this->isDebug)
            {
                if($assoc)
                    Mage::log("===================== ADDING PRODUCT BASIC DATA (this is Variation) PRODUCT (SKU={$aData['Product_ID']}) =====================", true, 'dropship.log');
                else
                    Mage::log("===================== ADDING PRODUCT BASIC DATA (SKU={$aData['Product_ID']}) =====================", true, 'dropship.log');

                $this->current_p_time = microtime(true);
            }

            $pid = $this->addProduct($aData, $visibility);
            sleep(2);
            return $pid;
        }
    }

    protected function addProduct($aData, $visibility = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
    {
        // stock is 0 for all at least test products so disabling this check for now
        if (!is_array($aData) || !count($aData)) // || !$aData['Stock'])
            return false;

        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        if(array_key_exists('PriceDiff', $aData))
            $price = $aData['PriceDiff'];
        else
            $price = $aData['Price'];

        if(array_key_exists('ListPrice', $aData))
            $msrp_price = $aData['ListPrice'];
        else
            $msrp_price = 0;

        $qty = $aData['Stock'];

        // skip importing simple products with price 0
        if(!empty($aData['Variations']) && intval($price) == 0)
            return;

        $catArr = array();

        // let's deal with categories
        if(array_key_exists('Categories', $aData))
            $catArr = $this->getCategoryIds($aData['Categories']);

        $sku = $aData['Product_ID'];
        $o_prod = Mage::getModel('catalog/product');

        if(!empty($aData['Variations']) and count($aData['Variations']) > 1)
        {
            $type = 'configurable';
        }else{
            $type = 'simple';
        }

        if(isset($aData['attribute_set_id']) and $aData['attribute_set_id'])
            $attr_set_id = $aData['attribute_set_id'];
        else
            $attr_set_id = $this->idAttrSet;

        $o_prod->setSku($sku)
            ->setDsDropshipProductId($sku)
            ->setWebsiteIds(array(1))
            ->setAttributeSetId($attr_set_id)
            ->setTypeId($type) //product type
            ->setCreatedAt(strtotime('now')) //product creation time
            ->setUpdatedAt(strtotime('now')) //product update time
            ->setTaxClassId(0)
            ->setStatus(1)
            ->setVisibility($visibility)
            ->setName($aData['Title'])
            ->setDescription($aData['Description'])
            ->setShortDescription($aData['Title'])
            ->setWeight(1)
            ->setPrice(preg_replace('/[^0-9\.]/', '', $price))
            ->setMsrp(preg_replace('/[^0-9\.]/', '', $msrp_price))
            ->setStockData(array('is_in_stock' => $qty ? 1 : 0, 'qty' => $qty));

        if (!empty($catArr))
            $o_prod->setCategoryIds($catArr);

        if(!empty($aData['Manufacturer'])) $o_prod->setDsManufacturer($aData['Manufacturer']);

        if(!empty($aData['Manufacturer_Logo']))
        {
            // first - save logo file
            $logo_img = $this->saveLogoImage($aData['Manufacturer_Logo']);
            $o_prod->setDsManufacturerLogo($logo_img);
        }

        $i = 0;

        if(array_key_exists('Attributes', $aData))
        {
            foreach($aData['Attributes'] as $attribute)
            {
                $code = self::ATTR_PREFIX . str_replace(' ', '_', preg_replace('/[^a-z ]/','', strtolower($attribute['name'])));

                // attribute code must not be longer then 30 chars
                $code = substr($code, 0, 29);

                $com = 'set' . str_replace(' ', '', ucwords((str_replace('_', ' ', $code))));

                // get or create attribute option if it doesn't exist
                $a_val = $this->getProductAttributeValue($code, $attribute['value'], $attribute['value']);

                $o_prod->{$com}($a_val);
            }
        }

        foreach(self::$A_ATTRS as $attr_c => $value_c)
        {
            $data_key = str_replace(' ', '', ucwords(str_replace('_', ' ', $attr_c)));

            if(!empty($aData[$data_key]))
            {
                $com = 'setDs' . $data_key;

                if($attr_c == 'manufacturer')
                    $a_val = $this->getProductAttributeValue('ds_manufacturer', $aData[$data_key], $aData[$data_key]);
                else
                    $a_val = $aData[$data_key];

                $o_prod->{$com}($a_val);
            }
        }

        if(!empty($aData['CalculatedShipping']) || $aData['CalculatedShipping'] === '0' || $aData['CalculatedShipping'] === 0)
            $o_prod->setWeight($aData['CalculatedShipping']);

        if(array_key_exists('Variations', $aData) && count($aData['Variations']) == 1)
        {
            $vData = $aData['Variations'][0];
            // a case where there's only one variation
            foreach($vData['Attributes'] as $vattr)
            {
                $code = self::ATTR_PREFIX . str_replace(' ', '_', preg_replace('/[^a-z ]/','', strtolower($vattr['name'])));

                // attribute code must not be longer then 30 chars
                $code = substr($code, 0, 29);

                $com = 'set' . str_replace(' ', '', ucwords((str_replace('_', ' ', $code))));
                $o_prod->{$com}($vattr['value']);
            }
        }

        $attrText = '';
        // now we set the attributes
        if(array_key_exists('Attributes', $aData))
            foreach($aData['Attributes'] as $mattr)
                $attrText .= $mattr['name'].': '.$mattr['value']."\n";

        if(!empty($attrText))
            $o_prod->setDsAttributes($attrText);

        try{
            $o_prod->save();
        } catch (Exception $e) {
            Mage::log('File: CpDevelopment_Dropship_Model_Sync, line: '. __LINE__ . ', exception: ' . $e->getMessage());
        }

        $pid = $o_prod->getId();
        $this->addImages($pid, $aData);

        if($this->isDebug)
        {
            Mage::log("Magento ID #" . $o_prod->getId().": FINISHED PROCESSING PRODUCT (SKU={$aData['Product_ID']}) | Processing time: " . (microtime(true) - $this->current_p_time) . 's', true, 'dropship.log');
            Mage::log("===================== END =====================", true, 'dropship.log');
        }

        if(!empty($aData['Variations']) and count($aData['Variations']) > 1)
        {
            $configAttrId = $this->variationAttributes[self::ATTR_PREFIX. 'variations_' . $aData['Product_ID']]['attrId'];
            $configProd = Mage::getModel('catalog/product')->load($pid);
            $configProd->setCanSaveConfigurableAttributes(true);
            $configurableProductsData = array();

            if($this->isDebug)
                $fTime = microtime(true);

            foreach($aData['Variations'] as $associatedProduct)
            {
                // first we add the missing information for associated product (title, description ... ) from main config product
                if(!isset($associatedProduct['Title']))
                    $associatedProduct['Title'] = $aData['Title'] . ' - ' . $associatedProduct['Product_ID'];
                if(!isset($associatedProduct['Description']))
                    $associatedProduct['Description'] = $aData['Description'];

                $attrVal =  $this->variationAttributes[self::ATTR_PREFIX. 'variations_' . $aData['Product_ID']]['values'][$associatedProduct['Product_ID']];
                $associatedProduct['attribute_set_id'] = $this->findAttrSet(self::ATTR_SET_NAME . ' - ' . $aData['Product_ID']);
                $simpleProductId = $this->procProduct($associatedProduct, Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE, true);

                if(!empty($simpleProductId))
                {
                    //echo "Added associated product with id: $simpleProductId\n";
                    $new_object = Mage::getModel('catalog/product')->load($simpleProductId);

                    try{
                        $acode = self::ATTR_PREFIX. 'variations_' . $aData['Product_ID'];

                        // we also save parent weight if it doesn't exist with variation
                        if(empty($associatedProduct['CalculatedShipping']))
                            $new_object->setWeight($aData['CalculatedShipping']);

                        $new_object->setData($acode, "$attrVal")->save();
                    } catch (Exception $e) {
                        echo $e->getMessage(),"\n";
                    }
                }

                $configurableProductsData[$simpleProductId] = array(
                    'label' => $associatedProduct['Title'],
                    'attribute_id' => $configAttrId,
                    'value_index' => $attrVal,
                    'is_percent' => false,
                    'pricing_value' => intval($associatedProduct['PriceDiff'])
                );

                $configurableAttributesData[0]['values'][] = array(
                    'label' => $associatedProduct['Title'],
                    'attribute_id' => $configAttrId,
                    'value_index' => $attrVal,
                    'is_percent' => false,
                    'pricing_value' => intval($associatedProduct['PriceDiff'])
                );
            }

            if($this->isDebug)
            {
                Mage::log("############ ADDING CONFIGURABLE DATA (SKU={$aData['Product_ID']}) ##############", true, 'dropship.log');
                $fTime = microtime(true);
            }

            $configurableAttributesData[0]['attribute_id'] = $configAttrId;
            $configProd->setConfigurableProductsData($configurableProductsData);
            $configProd->setConfigurableAttributesData($configurableAttributesData);
            $configProd->save();

            $stockItem = Mage::getModel('cataloginventory/stock_item');
            $stockItem->assignProduct($configProd);
            $stockItem->setData('is_in_stock', 1);
            $stockItem->setData('manage_stock', 1);
            $stockItem->setData('use_config_manage_stock', 0);
            $stockItem->setData('min_sale_qty', 1);
            $stockItem->setData('use_config_min_sale_qty', 0);
            $stockItem->setData('max_sale_qty', 1000);
            $stockItem->setData('use_config_max_sale_qty', 0);
            $stockItem->setData('qty', 10);
            $stockItem->save();

            if($this->isDebug)
            {
                Mage::log("Time spent here: " . (microtime(true) - $fTime) . "s", true, 'dropship.log');
                Mage::log("############ END ##############", true, 'dropship.log');
                $fTime = microtime(true);
            }

            return $configProd->getId();
        }else{
            return $pid;
        }

    }

    /**
     * Retrieves an array of magento category ids the product may belong to based on product
     * ds category ids
     *
     * @param array $dsCategories
     * @return array
     */
    protected function getCategoryIds($dsCategories)
    {
        $category_ids = array();

        foreach($dsCategories as $dsCatId)
        {
            foreach($this->_ds_categories as $magentoCatId => $dsConcatCategory)
            {
                $firstPID = $dsConcatCategory;

                if(strstr($dsConcatCategory, '.'))
                    $firstPID = substr($dsConcatCategory, 0, strpos($dsConcatCategory, '.'));

                if($firstPID == $dsCatId)
                    $category_ids[] = $magentoCatId;
            }

            if(empty($category_ids))
                Mage::log('ANOMALY: Something wrong, ds category id for product does not exist in Magento (field Ds Category Id under General tab in category edit page). Category ID: ' . $dsCatId);
        }

        return $category_ids;
    }

    /**
     * Removes attributes from product attribute set if they're no longer present in products ['Attributes'] data
     *
     * IMPORTANT: Using this function is fine as long as all products have their own attribute sets, otherwise,
     *            deleting attribute from a set will affect all products within the same attribute set
     *
     * @param array $product_data_keys
     * @param array $attribute_codes
     * @param int $productAttrSetId
     * @return void
     */
    protected function removeMissingAttributes($product_data_keys, $attribute_codes, $productAttrSetId)
    {
        /* test case: product with sku: 212659
         * removed attributes: {"name":"Compatible progressifs","value":"oui"},{"name":"Largeur de la face","value":"139 mm"},{"name":"Longueur des branches","value":"145 mm"},
         */
        $exclude_string = '';

        foreach(self::$A_ATTRS as $attr_c => $value_c)
            $exclude_string .= '|^ds_' . $attr_c;

        foreach($product_data_keys as $key => $attr_code)
        {
            // we check only atributes that should be in 'Attributes' array, with a few exceptions separated with | in this regex
            // so this if removes attributes that shouldn't be checked (Magento system attributes as well as few DS specific attrs)
            if(preg_match('/^ds_variations_'.$exclude_string.'|^(?:(?!ds_).)/is', $attr_code))
                unset($product_data_keys[$key]);
        }

        foreach($product_data_keys as $existing_attr)
        {
            if(!in_array($existing_attr, $attribute_codes))
            {
                try{
                    $attrId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', $existing_attr);
                    Mage::getModel('catalog/product_attribute_set_api')->attributeRemove($attrId, $productAttrSetId);
                } catch (Exception $e){
                    Mage::log('Failed to remove discontinued attribute from product. Attribute code: ' . $existing_attr . ', Attribut set ID: ' . $productAttrSetId . '. Reported error was: ' . $e->getMessage(), true, 'dropship-error.log');
                }
            }
        }

    }

    protected function updateProduct($product_id, $aData, $visibility = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
    {
        $o_prod = Mage::getModel('catalog/product')->load($product_id);

        // first check if the product types differ
        if(
            ($o_prod->getTypeID() == 'simple' && !empty($aData['Variations']) && count($aData['Variations']) > 1)
            ||
            ($o_prod->getTypeID() == 'configurable' && ( empty($aData['Variations']) or count($aData['Variations']) < 2 ) ) )
        {
            // we reccreate the product as we can't have new product data be a configurable product and the old product is simple
            $o_prod->delete();
            return $this->addProduct($aData);
        }

        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        if(array_key_exists('PriceDiff', $aData))
            $price = $aData['PriceDiff'];
        else
            $price = $aData['Price'];

        $msrp_price = $aData['ListPrice'];
        $qty = $aData['Stock'];

        $catArr = array();

        // let's deal with categories
        if(array_key_exists('Categories', $aData))
            $catArr = $this->getCategoryIds($aData['Categories']);

        $o_prod->setUpdatedAt(strtotime('now')) //product update time
            ->setDsDropshipProductId($aData['Product_ID'])
            ->setName($aData['Title'])
            ->setDescription($aData['Description'])
            ->setShortDescription($aData['Title'])
            ->setWeight(1)
            ->setPrice(preg_replace('/[^0-9\.]/', '', $price))
            ->setMsrp(preg_replace('/[^0-9\.]/', '', $msrp_price))
            ->setStockData(array('is_in_stock' => $qty ? 1 : 0, 'qty' => $qty));

        if (!empty($catArr))
            $o_prod->setCategoryIds($catArr);

        foreach($aData['Attributes'] as $attribute)
        {
            $code = self::ATTR_PREFIX . str_replace(' ', '_', preg_replace('/[^a-z ]/','', strtolower($attribute['name'])));

            // attribute code must not be longer then 30 chars
            $code = substr($code, 0, 29);
            // We keep this list of attributes so we can remove from attribute set those not used anymore
            $attribute_codes[] = $code;

            $com = 'set' . str_replace(' ', '', ucwords((str_replace('_', ' ', $code))));

            // get or create attribute option if it doesn't exist
            $a_val = $this->getProductAttributeValue($code, $attribute['value'], $attribute['value']);

            $o_prod->{$com}($a_val);
        }

        // remove any attributes that are no longer present in the 'Attributes' array
        $this->removeMissingAttributes(array_keys($o_prod->getData()), $attribute_codes, $o_prod->getAttributeSetId());

        foreach(self::$A_ATTRS as $attr_c => $value_c)
        {
            $data_key = str_replace(' ', '', ucwords(str_replace('_', ' ', $attr_c)));

            if(!empty($aData[$data_key]))
            {
                $com = 'setDs' . $data_key;

                if($attr_c == 'manufacturer')
                    $a_val = $this->getProductAttributeValue('ds_manufacturer', $aData[$data_key], $aData[$data_key]);
                else
                    $a_val = $aData[$data_key];

                $o_prod->{$com}($a_val);
            }
        }

        if(!empty($aData['CalculatedShipping']) || $aData['CalculatedShipping'] === '0' || $aData['CalculatedShipping'] === 0)
            $o_prod->setWeight($aData['CalculatedShipping']);

        if(!empty($aData['Manufacturer'])) $o_prod->setDsManufacturer($aData['Manufacturer']);

        if(!empty($aData['Manufacturer_Logo']))
        {
            // first - save logo file
            $logo_img = $this->saveLogoImage($aData['Manufacturer_Logo']);
            $o_prod->setDsManufacturerLogo($logo_img);
        }

        $image_arr = array();
        foreach($aData['Images'] as $img)
            $image_arr[] = $img;

        // adding any variation images to main configurable image
        foreach($aData['Variations'] as $vImages)
        {
            foreach($vImages['Images'] as $vimg)
            {
                $vImgUrl = $vimg;

                if(in_array($vImgUrl, $image_arr))
                    continue;
                else
                    $image_arr[] = $vImgUrl;
            }
        }

        $aData['Images'] = $image_arr;

        try{
            $o_prod->save();
        } catch (Exception $e) {
            Mage::log('File: CpDevelopment_Dropship_Model_Sync, line: '. __LINE__ . ', exception: ' . $e->getMessage());
            echo $e->getMessage();
        }

        // Sync of images
        $this->syncImages($o_prod->getId(), $aData['Images']);

        $pid = $product_id;

        if(!empty($aData['Variations']) and count($aData['Variations']) > 1)
        {
            // break all of the existing associations
            Mage::getResourceSingleton('catalog/product_type_configurable')
                ->saveProducts($o_prod, array());

            $configProd = Mage::getModel('catalog/product')->load($pid);
            $configAttrId = $this->variationAttributes[self::ATTR_PREFIX. 'variations_' . $aData['Product_ID']]['attrId'];

            // remove all of the existing pricing super attributes
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $query = "DELETE FROM `catalog_product_super_attribute` WHERE `attribute_id` = " . (int) $configAttrId;
            $writeConnection->query($query);

            $configProd->setCanSaveConfigurableAttributes(true);
            $configurableProductsData = array();

            foreach($aData['Variations'] as $associatedProduct)
            {
                // first we add the missing information for associated product (title, description ... ) from main config product
                if(!isset($associatedProduct['Title']))
                    $associatedProduct['Title'] = $aData['Title'] . ' - ' . $associatedProduct['Product_ID'];
                if(!isset($associatedProduct['Description']))
                    $associatedProduct['Description'] = $aData['Description'];

                $attrVal =  $this->variationAttributes[self::ATTR_PREFIX. 'variations_' . $aData['Product_ID']]['values'][$associatedProduct['Product_ID']];
                $associatedProduct['attribute_set_id'] = $this->findAttrSet(self::ATTR_SET_NAME . ' - ' . $aData['Product_ID']);
                $simpleProductId = $this->procProduct($associatedProduct, Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE, $aData['Product_ID']);

                if(!empty($simpleProductId) and !in_array($simpleProductId, $childProductIds))
                {
                    //echo "Added associated product with id: $simpleProductId\n";
                    $new_object = Mage::getModel('catalog/product')->load($simpleProductId);

                    try{
                        $acode = self::ATTR_PREFIX. 'variations_' . $aData['Product_ID'];

                        // we also save parent weight if it doesn't exist with variation
                        if(empty($associatedProduct['CalculatedShipping']))
                            $new_object->setWeight($aData['CalculatedShipping']);

                        $new_object->setData($acode, "$attrVal")->save();
                    } catch (Exception $e) {
                        echo $e->getMessage(),"\n";
                    }
                }

                $configurableProductsData[$simpleProductId] = array(
                    'label' => $associatedProduct['Title'],
                    'attribute_id' => $configAttrId,
                    'value_index' => $attrVal,
                    'is_percent' => false,
                    'pricing_value' => intval($associatedProduct['PriceDiff'])
                );

                $configurableAttributesData[0]['values'][] = array(
                    'label' => $associatedProduct['Title'],
                    'attribute_id' => $configAttrId,
                    'value_index' => $attrVal,
                    'is_percent' => false,
                    'pricing_value' => intval($associatedProduct['PriceDiff'])
                );
            }

            $configurableAttributesData[0]['attribute_id'] = $configAttrId;
            $configProd->setConfigurableProductsData($configurableProductsData);
            $configProd->setConfigurableAttributesData($configurableAttributesData);
            $configProd->save();

            $stockItem = Mage::getModel('cataloginventory/stock_item');
            $stockItem->assignProduct($configProd);
            $stockItem->setData('is_in_stock', 1);
            $stockItem->setData('manage_stock', 1);
            $stockItem->setData('use_config_manage_stock', 0);
            $stockItem->setData('min_sale_qty', 1);
            $stockItem->setData('use_config_min_sale_qty', 0);
            $stockItem->setData('max_sale_qty', 1000);
            $stockItem->setData('use_config_max_sale_qty', 0);
            $stockItem->setData('qty', 10);
            $stockItem->save();

            return $configProd->getId();
        }elseif(count($aData['Variations']) == 1){
            $vData = $aData['Variations'][0];
            // a case where there's only one variation
            foreach($vData['Attributes'] as $vattr)
            {
                $code = self::ATTR_PREFIX . str_replace(' ', '_', preg_replace('/[^a-z ]/','', strtolower($vattr['name'])));

                // attribute code must not be longer then 30 chars
                $code = substr($code, 0, 29);

                $com = 'set' . str_replace(' ', '', ucwords((str_replace('_', ' ', $code))));
                $o_prod->{$com}($vattr['value']);
            }

            try{
                $o_prod->save();
            } catch (Exception $e) {
                Mage::log('File: CpDevelopment_Dropship_Model_Sync, line: '. __LINE__ . ', exception: ' . $e->getMessage());
            }
        }else{
            return $pid;
        }
    }

    /**
     * Checks if product already has the image on $imgUrl url address and if not, adds it to product.
     *
     * @param Mage_Catalog_Product $product
     * @param string $imgUrl
     * @return void
     */
    protected function syncImages($product_id, $images)
    {
        if(empty($images))
            return;

        $product = Mage::getModel('catalog/product')->load($product_id);

        $eHashes = array();
        $hashes = array();

        foreach($images as $imgUrl)
        {
            $fname = substr($imgUrl, strrpos($imgUrl, '/') + 1);
            $new_images[$imgUrl] = substr($fname, 0, strrpos($fname, '.'));
        }

        $existingImages = $product->getMediaGalleryImages();
        $eImage = array();
        $reload = false;

        // Deleting existing images that do not exist in $images array
        foreach ($existingImages as $eImg)
        {
            $imageData = $eImg->getData();
            $cname = substr($imageData['file'], 0, strrpos($imageData['file'], '.'));
            $cname = substr($cname, strrpos($cname, '/') + 1);
            $delete = true;

            // check if any of the images can be considered the same as the existing ones
            foreach($new_images as $url => $nimg)
            {
                if(stristr($cname, $nimg))
                {
                    $delete = false;
                    unset($new_images[$url]);
                    break;
                }
            }

            if($delete && !empty($new_images))
            {
                $remove=Mage::getModel('catalog/product_attribute_media_api')->remove($product->getId(),$eImg['file']);
                exec("rm -f ".$eImg['path']);
                //echo "Deleting existing image ... {$imageData['path']}\n";
                $reload = true;
            }else
                $eImage[] = $imageData['path']; // this array holds all the hashes of existing images (not deleted)
        }

        // need to reload product object when images were deleted as otherwise mysql integrity constraint is thrown
        if($reload)
            $product = Mage::getModel('catalog/product')->load($product_id);

        // if there're no images attached to product, we'll assign the first one of the new images as default ones
        if(count($eImage))
            $attrs = array();
        else
            $attrs = array('image','thumbnail','small_image');

        foreach($new_images as $url => $fname)
        {
            //$fname = substr($url, strrpos($url, '/') + 1);
            $path = $this->saveImage($url);

            if($path && file_exists($path))
            {
                //echo "Adding new image ... $path\n";
                $product->addImageToMediaGallery($path, $attrs, false, false);
                $attrs = array(); // reseting this array so that the first new image becomes default
            }
        }


        try{
            $product->save();
        } catch (Exception $e) {
            Mage::log('File: CpDevelopment_Dropship_Model_Sync, line: '. __LINE__ . ', exception: ' . $e->getMessage());
            echo $e->getMessage();
        }
    }

    protected function findCategory($name) {
        $o_coll = Mage::getModel('catalog/category')->getCollection()
            ->setStoreId(0)
            ->addAttributeToFilter('is_active',1)
            ->addAttributeToFilter('name',$name);
        $a_ids = $o_coll->getAllIds();
        Mage::log("findCategory(): name='$name'; a_ids: ".rtrim(print_r($a_ids,true))."\n");
        return is_array($a_ids) && count($a_ids)? $a_ids[0] : false;
    }

    protected function _getIndexers()
    {
        return Mage::getSingleton('index/indexer')->getProcessesCollection();
    }

    protected function _stopReindex()
    {
        $this->_getIndexers()->walk('setMode', array(Mage_Index_Model_Process::MODE_MANUAL));
        $this->_getIndexers()->walk('save');
    }

    protected function _startReindex()
    {
        $this->_getIndexers()->walk('reindexAll');
        $this->_getIndexers()->walk('setMode', array(Mage_Index_Model_Process::MODE_REAL_TIME));
        $this->_getIndexers()->walk('save');
    }

    protected function getRsvAttrName($i)
    {
        $a_names = array_keys(self::$A_ATTRS);
        return $a_names[$i];
    }

    protected function createAttr($code, $label, $aData, $addSetGroup = true)
    {
        $a_data = array(
            'attribute_code' => $code,
            'is_global' => '1',
            'frontend_input' => 'text',
            'default_value_text' => '',
            'default_value_yesno' => '0',
            'default_value_date' => '',
            'default_value_textarea' => '',
            'is_unique' => '0',
            'is_required' => '0',
            'apply_to' => array('simple'),
            'is_configurable' => '0',
            'is_searchable' => '0',
            'is_visible_in_advanced_search' => '0',
            'is_filterable' => '0',
            'is_filterable_in_search' => '0',
            'is_comparable' => '0',
            'is_used_for_price_rules' => '0',
            'is_wysiwyg_enabled' => '0',
            'is_html_allowed_on_front' => '1',
            'is_visible_on_front' => '0',
            'used_in_product_listing' => '0',
            'used_for_sort_by' => '0',
            'frontend_label' => array($label),
            'default_value' => '',
        );

        if ($aData)
            $a_data = array_merge($a_data, $aData);

        $type = $a_data['frontend_input'];
        $o_attr = Mage::getModel('catalog/resource_eav_attribute');

        if (is_null($o_attr->getIsUserDefined()) || $o_attr->getIsUserDefined() != 0)
            $a_data['backend_type'] = $o_attr->getBackendTypeByInput($type);

        Mage::log("createAttr(): code='$code'; label='$label'; type='$type'\n a_data: ".rtrim(print_r($a_data,true))."\n", true, 'martin.log');

        $o_attr->addData($a_data);
        $o_attr->setEntityTypeId(Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId());
        $o_attr->setIsUserDefined(1);

        try {
            $o_attr->save();
        } catch (Exception $e) {
            Mage::log("Exception: msg: ".$e->getMessage());
            return false;
        }

        $id = $o_attr->getId();

        if (!$id) return false;

        if($addSetGroup)
            Mage::getModel('eav/entity_setup','core_setup')->addAttributeToSet('catalog_product',$this->idAttrSet,$this->idAttrGroup,$id);

        return $id;
    }

    protected function findAttrSet($name) {
        $id = Mage::getModel('eav/entity_attribute_set')
            ->load($name, 'attribute_set_name')->getAttributeSetId();
        //Mage::log("findAttrSet(): id='$id'\n");
        return $id? $id : false;
    }

    protected function createAttrSet($name, $basedOn = 'Default')
    {
        $id_entitytype = Mage::getModel('catalog/product')->getResource()->getTypeId();
        $id_deft = Mage::getModel('eav/entity_setup','core_setup')->getAttributeSetId($id_entitytype, $basedOn);
        $o_attrset = Mage::getModel('eav/entity_attribute_set');
        $o_attrset->setEntityTypeId($id_entitytype);
        $o_attrset->setAttributeSetName($name);

        if (!$o_attrset->validate())
            return false;

        try {
            $o_attrset->save();
            $id = $o_attrset->getId();

            if(!$id)
                return false;

            $o_attrset->initFromSkeleton($id_deft);
            $o_attrset->save();
            return $id;
        } catch (Exception $e) {
            Mage::log("Exception: msg: ".$e->getMessage());
            return false;
        }
    }

    protected function findAttrGroup($name, $setId = 0)
    {
        if(!$setId && $this->idAttrSet)
            $setId = $this->idAttrSet;

        $group = Mage::getModel('eav/entity_attribute_group')->getCollection()
            ->addFieldToFilter('attribute_group_name', $name)
            ->addFieldToFilter('attribute_set_id', $setId)->getFirstItem();

        $id = $group->getAttributeGroupId();

        return $id? $id : false;
    }

    protected function createAttrGroup($name, $idSet)
    {
        if(!$idSet)
            return false;

        $o_attrgrp = Mage::getModel('eav/entity_attribute_group');
        $o_attrgrp->setAttributeGroupName($name)
            ->setAttributeSetId($idSet)
            ->setSortOrder(100);

        try {
            $o_attrgrp->save();
            return $o_attrgrp->getId();
        } catch (Exception $e) {
            Mage::log("Exception: msg: ".$e->getMessage());
            return false;
        }
    }

    protected function addAttrOptions($sAttrCode, $aLabels) {
        if (!is_array($aLabels) || !count($aLabels) || !isset($this->aAttrOptionsets[$sAttrCode])) return false;
        Mage::log("addAttrOptions(): sAttrCode='$sAttrCode'; aLabels:".rtrim(print_r($aLabels,true))."\n");
        $ra_options = &$this->aAttrOptionsets[$sAttrCode];
        $a_ids = array();
        foreach ($aLabels as $s) {
            if ($this->addAttrOption($sAttrCode, $s, $ra_options))
                $this->loadAttrOptions($sAttrCode);
            if (isset($ra_options[$s])) $a_ids[$ra_options[$s]] = $s;
        }
        Mage::log(" a_ids:".rtrim(print_r($a_ids,true))."\n");
        return count($a_ids)? array_keys($a_ids) : false;
    }

    protected function addAttrOption($sAttrCode, $sLabel, $aOptions) {
        if (!is_array($aOptions))
            return false;
        if (isset($aOptions[$sLabel]))
            return false;
        if (!$sAttrCode)
            return false;
        $sAttrId = Mage::getResourceModel('eav/entity_attribute')
            ->getIdByCode('catalog_product', $sAttrCode);
        if (!$sAttrId)
            return false;
        $n_opts = count($aOptions);
        Mage::log("addAttrOption(): sAttrCode='$sAttrCode'; sAttrId='$sAttrId'; n_opts='$n_opts'; sLabel='$sLabel'\n");
        $o_attrinfo = Mage::getModel('eav/entity_attribute')->load($sAttrId);
        $value['option'] = array($sLabel,$sLabel);
        $order['option'] = $n_opts+ 1;
        $result = array('value' => $value, 'order' => $order);
        $o_attrinfo->setData('option',$result);
        $o_attrinfo->save();
        return true;
    }

    protected function loadAttrId($sAttrCode) {
        if (isset($this->aAttrIds[$sAttrCode])) return $this->aAttrIds[$sAttrCode];
        $id = Mage::getResourceModel('eav/entity_attribute')
            ->getIdByCode('catalog_product', $sAttrCode);
        if (!$id) return false;
        return $this->aAttrIds[$sAttrCode] = $id;
    }

    protected function loadAttrOptions($sAttrCode) {
        if (!$sAttrCode)
            return false;
        $sAttrId = $this->loadAttrId($sAttrCode);
        if (!$sAttrId)
            return false;
        $a_opts = Mage::getModel('catalog/resource_eav_attribute')
            ->load($sAttrId)
            ->getSource()->getAllOptions();
        Mage::log("loadAttrOptions(): sAttrCode='$sAttrCode'; sAttrId='$sAttrId'\n a_opts(" . count($a_opts) . "):" . print_r($a_opts, true) . "\n");
        if (!count($a_opts))
            return false;
        $a_ret = array();
        foreach ($a_opts as $arr) {
            if (isset($arr['label']) && $arr['label'])
                $a_ret[$arr['label']] = $arr['value'];
        }
        Mage::log(" a_ret:".rtrim(print_r($a_ret,true))."\n");
        $this->aAttrOptionsets[$sAttrCode] = $a_ret;
        return $a_ret;
    }

    protected function saveImage($url)
    {
        if (!$url)
            return false;

        $url = str_replace('http:\/\/https', 'http', $url);
        $url = str_replace('http://https', 'http', $url);

        if(!stristr($url, 'http'))
            $url = 'http://' . rtrim(str_replace('\/', '/', $url), 'http://');

        $url = str_replace('\/', '/', $url);

        //echo $url,"\n";
        $url = str_replace(' ', '%20', $url);

        $fTime = microtime(true);
        $cont = file_get_contents($url);
        //Mage::log("\tDownloaded product image ($url) in: " . (microtime(true) - $fTime) . 's', true, 'dropship.log');

        if (!$cont)
            return false;

        $fname = substr($url, strrpos($url, '/') + 1);
        $fname = str_replace('?dl=1', '', $fname);

        // we're accounting here for possibility that image urls have parameters and were saved with such parameters
        if(stristr($fname, '?'))
        {
            $new_fname = substr($fname, 0, strrpos($fname, '?'));
            exec("mv " . Mage::getBaseDir('tmp') . DS . $fname . ' ' . Mage::getBaseDir('tmp') . DS . $new_fname);
            $fname = $new_fname;
        }

        $filepath = Mage::getBaseDir('tmp') . DS . $fname;

        file_put_contents($filepath, $cont);

        return $filepath;
    }

    protected function saveLogoImage($url)
    {
        if (!$url)
            return false;

        $cont = file_get_contents($url);

        if (!$cont)
            return false;

        $fname = substr($url, strrpos($url, '/') + 1);
        $filepath = Mage::getBaseDir('media'). DS . self::MANUFACTURER_LOGO_DIR . DS . $fname;
        file_put_contents($filepath, $cont);

        return DS . 'media' . DS . self::MANUFACTURER_LOGO_DIR . DS . $fname;;
    }
}
