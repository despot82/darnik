<?php
$installer = $this;
$installer->startSetup();

if (!file_exists(Mage::getBaseDir() . DS . 'media' . DS . 'manufacturer-logo')) {
    mkdir(Mage::getBaseDir() . DS . 'media' . DS . 'manufacturer-logo', 0777, true);
}

$installer->endSetup();