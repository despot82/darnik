<?php
$installer = $this;
$installer->startSetup();
$this->addAttribute(
    'catalog_product',
    'ds_is_product_synced',
    array(
        'group' => 'General Information',
        'input' => 'boolean',
        'type' => 'int',
        'source' => 'eav/entity_attribute_source_boolean',
        'label' => 'Is product url synced to Dropshipexplorer?',
        'default' => 0,
        'required' => 0,
        'unique' => 0,
        'sort_order' => 3,
        'user_defined' => 1,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    ));


$attSet = Mage::getModel('eav/entity_type')->getCollection()->addFieldToFilter('entity_type_code','catalog_product')->getFirstItem(); // This is because the you adding the attribute to catalog_products entity ( there is different entities in magento ex : catalog_category, order,invoice... etc ) 
$attSetCollection = Mage::getModel('eav/entity_type')->load($attSet->getId())->getAttributeSetCollection(); // this is the attribute sets associated with this entity 


$attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
    ->setCodeFilter('ds_is_product_synced')
    ->getFirstItem();
$attCode = $attributeInfo->getAttributeCode();
$attId = $attributeInfo->getId();

foreach ($attSetCollection as $a)
{
    $set = Mage::getModel('eav/entity_attribute_set')->load($a->getId());
    $setId = $set->getId();
    $group = Mage::getModel('eav/entity_attribute_group')->getCollection()->addFieldToFilter('attribute_set_id',$setId)->setOrder('attribute_group_id',"ASC")->getFirstItem();
    $groupId = $group->getId();
    $newItem = Mage::getModel('eav/entity_attribute');
    $newItem->setEntityTypeId($attSet->getId()) // catalog_product eav_entity_type id ( usually 10 )
              ->setAttributeSetId($setId) // Attribute Set ID
              ->setAttributeGroupId($groupId) // Attribute Group ID ( usually general or whatever based on the query i automate to get the first attribute group in each attribute set )
              ->setAttributeId($attId) // Attribute ID that need to be added manually
              ->setSortOrder(10) // Sort Order for the attribute in the tab form edit
              ->save();
}

$installer->endSetup();
?>