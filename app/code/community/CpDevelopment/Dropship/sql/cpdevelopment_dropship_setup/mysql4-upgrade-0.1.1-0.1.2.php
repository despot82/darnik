<?php
$installer = $this;
$installer->startSetup();

if(!Mage::getStoreConfig('carriers/tablerate/active'))
    Mage::getConfig()->saveConfig('carriers/tablerate/active', '1', 'default', 0);

Mage::getConfig()->saveConfig('carriers/tablerate/condition_name', 'package_weight', 'default', 0);

// insert table rate that will be 1 $,euro... for every destination for 1 kg, lb, ...
// so any value entered in the 'Weight' for product will actually be a shipping price for that product
$installer->run("TRUNCATE `shipping_tablerate`;");

$websites = Mage::getModel('core/website')->getCollection();

foreach ($websites as $website)
{
    $webId = $website->getId();
    // this is to allow free shipping on zero weight products
    $values = "('".$webId."', 'US', '0', '*', 'package_weight', '0', '0', '0.0000'),";
    $weight = 1;

	while ($weight <= 1000) {

		if($weight < 11)
			$rate = $weight;
		elseif ($weight > 10 and $weight < 16)
			$rate = max(10, intval($weight * 0.90));
		elseif ($weight > 15 and $weight < 21)
			$rate = max(intval($weight*0.85), 14);
		elseif ($weight > 20 and $weight < 31)
			$rate = max(intval($weight*0.80), 17);
		elseif ($weight > 30 and $weight < 41)
			$rate = max(intval($weight*0.75), 26);
		elseif ($weight > 40 and $weight < 51)
			$rate = max(intval($weight*0.70), 32);
		elseif ($weight > 50 and $weight < 101)
			$rate = max(intval($weight*0.60), 38);
		elseif ($weight > 100 and $weight < 201)
			$rate = max(intval($weight*0.50), 60);
		elseif ($weight > 200 and $weight < 301)
			$rate = max(intval($weight*0.48), 100);
		elseif ($weight > 300 and $weight < 501)
			$rate = max(intval($weight*0.46), 145);
		elseif ($weight > 500)
			$rate = max(intval($weight*0.44), 230);


		$values .= "('".$webId."', 'US', '0', '*', 'package_weight', '".$weight."', '".$rate."', '0.0000'),";
		$weight++;
	}

	$values = rtrim($values,',');

    $installer->run("INSERT INTO `shipping_tablerate`  (`website_id`, `dest_country_id`, `dest_region_id`, `dest_zip`, `condition_name`, `condition_value`, `price`, `cost`) VALUES $values;");
}

$installer->endSetup();