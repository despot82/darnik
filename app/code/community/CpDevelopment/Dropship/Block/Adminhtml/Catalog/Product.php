<?php
class CpDevelopment_Dropship_Block_Adminhtml_Catalog_Product extends Mage_Adminhtml_Block_Catalog_Product
{
    public function __construct() {
        parent::__construct();

        if (!CpDevelopment_Dropship_Model_Config::isShowButton()) return;
        Mage::log("CpDevelopment_Dropship_Block_Adminhtml_Catalog_Product::__construct():\n\n");
        $this->_addButton('rundropship', array(
            'label'    => Mage::helper('catalog')->__('Run Dropship Import'),
            'onclick'  => 'setLocation(\'' . $this->getUrl('dropship/syncman/run') .'\')'
        ));
    }
}
