
var jQuery = jQuery.noConflict(); 

jQuery(document).ready(function(){
	
	//alert("Ready!");
	
	jQuery(":has(> #naziv_pravnog_lica)").hide();
	jQuery(":has(> #pib)").hide();
	
	jQuery(":has(> #kontakt_osoba)").hide();
	
	jQuery("#fizickolice").click(function(){
    	
    	jQuery(":has(> #ime)").show();
    	jQuery(":has(> #prezime)").show();
    	
    	jQuery(":has(> #naziv_pravnog_lica)").hide();
    	jQuery(":has(> #pib)").hide();
    	
    	jQuery(":has(> #kontakt_osoba)").hide();
    	
    });
    
	jQuery("#pravnolice").click(function(){
    	
    	jQuery(":has(> #ime)").hide();
    	jQuery(":has(> #prezime)").hide();
    	
    	jQuery(":has(> #naziv_pravnog_lica)").show();
    	jQuery(":has(> #pib)").show();
    	
    	jQuery(":has(> #kontakt_osoba)").show();
    
    	
    });
    
});